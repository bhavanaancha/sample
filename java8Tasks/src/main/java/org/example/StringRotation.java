package org.example;

public interface StringRotation{
    boolean isStringsRotationOfEachOther(String stringOne,String stringTwo);
}

