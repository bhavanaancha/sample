package org.example;

import java.util.List;

public interface SecondBigNumber{
    int findSecondLargestNumber(List<Integer> numbersList);
}

