package org.example;

public class Product {
    private String itemName;
    private double price;
    private String category;
    private String grade;

    public Product(String category, String item, double price, String grade) {
        this.category = category;
        this.itemName = item;
        this.price = price;
        this.grade = grade;
    }

    public Product() {

    }


    public String getProductName() {
        return itemName;
    }

    public void setProductName(String productName) {
        this.itemName = productName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }
    @Override
    public String toString() {
        return "Product{" +
                "category='" + category + '\'' +
                ", itemName='" + itemName + '\'' +
                ", price=" + price +
                ", grade=" + grade +
                '}';
    }
}
