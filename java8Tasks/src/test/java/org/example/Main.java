package org.example;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Palindrome object=(actualString)->{
            String stringOne=actualString.toLowerCase();
            String reverseString="";
            for(int index=actualString.length()-1;index>=0;index--){
                reverseString+=stringOne.charAt(index);
            }
            return reverseString.equals(stringOne);

        };
        boolean palindrome =object.checkPalindrome("madam");
        if(palindrome) System.out.println("String is a palindrome");
        else System.out.println("String is not a palindrome");

        List<Integer>numbersList= Arrays.asList(10,20,30,40,50,60);
        SecondBigNumber number= (numberList)->{
            Collections.sort(numberList);
            return numberList.get(numberList.size()-2);
        };
        System.out.println("Second largest number in the list is: "+
                number.findSecondLargestNumber(numbersList));

        StringRotation stringRotationObject=(firstString, secondString)->{
            String stringOne=firstString+firstString;
            return stringOne.contains(secondString);
        };
        boolean checkRotate=stringRotationObject.isStringsRotationOfEachOther("abcd","cdab");
        if(checkRotate) System.out.println("Two strings are rotations of each other");
        else System.out.println("Two strings are not rotations of each other");

        Thread myThread=new Thread((java.lang.Runnable) () -> {
            for(int index=1;index<=10;index++)
                System.out.print(index+" ");
        });
        myThread.start();


        Collections.sort(numbersList, (numberOne,numberTwo)->numberTwo.compareTo(numberOne));
        System.out.println();
        System.out.println("Reverse sort of numbers using Comparator: "+numbersList);
        List<String >namesList=Arrays.asList("Bhavana","Anushka","Virat","Dev","Bumrah","Shubhman");
        Collections.sort(namesList,(stringOne,stringTwo)->stringOne.compareTo(stringTwo));
        System.out.println("Employee names in alphabetical order using comparator: "+namesList);

        TreeSet<Integer>numberSet=new TreeSet<>((o1, o2) -> (o1 > o2) ? -1 : (o1 < o2) ? 1 : 0);

        for (Integer num : numbersList) {
            numberSet.add(num);
        }
        System.out.println("Reverse numbers using TreeSet: "+numberSet);

        TreeSet<String>namesSet=new TreeSet<>((firstString,secondString)->firstString.compareTo(secondString));

        for (String num : namesList) {
            namesSet.add(num);
        }
        System.out.println("Reverse names using TreeSet: "+namesSet);

        TreeMap<Integer,String>treeMap=new TreeMap<>((o1, o2) -> o2.compareTo(o1));
        treeMap.put(3,"Virat");
        treeMap.put(4,"Anushka");
        treeMap.put(1,"Ranbir");
        treeMap.put(2,"Rahul");
        treeMap.put(5,"Athiya");
        List<Map.Entry<Integer,String>>entries=new ArrayList<>(treeMap.entrySet());
        System.out.println("TreeMap that sorts the given set of values in descending order: ");
        Collections.sort(entries,(object1,object2)->object2.getKey()-(object1.getKey()));
        for(Map.Entry<Integer,String > entry:entries){
            System.out.print(entry.getKey()+": "+entry.getValue()+" ");
        }
        System.out.println();
        System.out.println("TreeMap that sorts the given set of employee names in descending order: ");

        Collections.sort(entries,(object1,object2)->object2.getValue().compareTo(object1.getValue()));
        for(Map.Entry<Integer,String > entry:entries){
            System.out.print(entry.getKey()+": "+entry.getValue()+" ");
        }
        System.out.println();
        Collections.sort(namesList,(stringOne,stringTwo)->stringTwo.compareTo(stringOne));
        System.out.println("Employee names in reverse alphabetical order: "+namesList);

    }

}
