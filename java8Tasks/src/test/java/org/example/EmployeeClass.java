package org.example;

import java.util.*;
import java.util.stream.Collectors;

public class EmployeeClass {
    public static void main(String[] args) {
        List<Employee> employeeList = new ArrayList<Employee>();
        employeeList.add(new Employee(111, "Jiya Brein", 32, "Female", "HR", 2011, 25000.0));
        employeeList.add(new Employee(122, "Paul Niksui", 25, "Male", "Sales And Marketing", 2015, 13500.0));
        employeeList.add(new Employee(133, "Martin Theron", 29, "Male", "Infrastructure", 2012, 18000.0));
        employeeList.add(new Employee(144, "Murali Gowda", 28, "Male", "Product Development", 2014, 32500.0));
        employeeList.add(new Employee(155, "Nima Roy", 27, "Female", "HR", 2013, 22700.0));
        employeeList.add(new Employee(166, "Iqbal Hussain", 43, "Male", "Security And Transport", 2016, 10500.0));
        employeeList.add(new Employee(177, "Manu Sharma", 35, "Male", "Account And Finance", 2010, 27000.0));
        employeeList.add(new Employee(188, "Wang Liu", 31, "Male", "Product Development", 2015, 34500.0));
        employeeList.add(new Employee(199, "Amelia Zoe", 24, "Female", "Sales And Marketing", 2016, 11500.0));
        employeeList.add(new Employee(200, "Jaden Dough", 38, "Male", "Security And Transport", 2015, 11000.5));
        employeeList.add(new Employee(211, "Jasna Kaur", 27, "Female", "Infrastructure", 2014, 15700.0));
        employeeList.add(new Employee(222, "Nitin Joshi", 25, "Male", "Product Development", 2016, 28200.0));
        employeeList.add(new Employee(233, "Jyothi Reddy", 27, "Female", "Account And Finance", 2013, 21300.0));
        employeeList.add(new Employee(244, "Nicolus Den", 24, "Male", "Sales And Marketing", 2017, 10700.5));
        employeeList.add(new Employee(255, "Ali Baig", 23, "Male", "Infrastructure", 2018, 12700.0));
        employeeList.add(new Employee(266, "Sanvi Pandey", 26, "Female", "Product Development", 2015, 28900.0));
        employeeList.add(new Employee(277, "Anuj Chettiar", 31, "Male", "Product Development", 2012, 35700.0));


        System.out.println("No of males and females are ");
        System.out.println("Males count is " + employeeList.stream().
                filter(gender -> gender.getGender().equals("Male")).count());
        System.out.println("Females count is " + employeeList.stream().
                filter(gender -> gender.getGender().equals("Female")).count());
        System.out.println("Names of all departments in the organization are ");
        employeeList.stream().map(dept -> dept.getDepartment()).distinct().forEach(System.out::println);
        System.out.println("Average ge of male and females employees is ");
        System.out.println(employeeList.stream().collect(
                Collectors.groupingBy(
                        employee -> employee.getGender(),
                        Collectors.averagingInt(employee -> employee.getAge()))));
        System.out.println("Highest paid employee in the organization is ");
        System.out.println(employeeList.stream()
                .max(Comparator.comparing(Employee::getSalary)).get());
        System.out.println("Names of employees who have joined after 2015: ");
        employeeList.stream().filter(year -> year.getYearOfJoining() > 2015).map(name -> name.getName())
                .forEach(System.out::println);
        System.out.println("No of employees in each department: ");
        System.out.println(employeeList.stream()
                .collect(Collectors.groupingBy(dept -> dept.getDepartment(), Collectors.counting())));
        System.out.println("Average salary of each department: ");
        System.out.println(employeeList.stream()
                .collect(Collectors.groupingBy(dept -> dept.getDepartment(), Collectors.averagingInt(Employee::getAge))));
        System.out.println("Youngest Male Employee in product Development details: ");
        System.out.println(employeeList.stream().filter(dept -> dept.getDepartment().equals("Product Development"))
                .filter(gender -> gender.getGender().equals("Male")).min(Comparator.comparing(Employee::getAge)).get());
        System.out.println("Most experienced employee in the organization: ");
        System.out.println(employeeList.stream().min(Comparator.comparing(Employee::getYearOfJoining)).get());
        System.out.println("No of males and females in Sales and Marketing are: ");
        Map<String, Long> countOfMalesAndFemales = employeeList.stream()
                .filter(dept -> dept.getDepartment().equals("Sales And Marketing"))
                .collect(Collectors.groupingBy(Employee::getGender, Collectors.counting()));
        System.out.println(countOfMalesAndFemales);
        System.out.println("Average salary of males and females: ");
        System.out.println(employeeList.stream().
                collect(Collectors.groupingBy(Employee::getGender, Collectors.averagingDouble(Employee::getSalary))));
        System.out.println("Names of employees in each department: ");
        Map<String, List<Employee>> employeeListByDepartment =
                employeeList.stream().collect(Collectors.groupingBy(Employee::getDepartment));
        List<Map.Entry<String, List<Employee>>> entrySet = new ArrayList<>(employeeListByDepartment.entrySet());
        for (Map.Entry<String, List<Employee>> entry : entrySet) {
            System.out.println("Employees In " + entry.getKey() + " : ");
            List<Employee> list = entry.getValue();
            for (Employee e : list) {
                System.out.println(e.getName());
            }
        }
        System.out.println("Average and total salary of employees: ");
        DoubleSummaryStatistics totalAvgSalary =
                employeeList.stream().collect(Collectors.summarizingDouble(Employee::getSalary));

        System.out.println("Average Salary: " + totalAvgSalary.getAverage());

        System.out.println("Total Salary: " + totalAvgSalary.getSum());

        System.out.println("Employees who are below 25 years: ");
        employeeList.stream().filter(age -> age.getAge() <= 25).map(name -> name.getName()).forEach(System.out::println);
        System.out.println("Employees who are above 25 years: ");
        employeeList.stream().filter(age -> age.getAge() > 25).map(name -> name.getName()).forEach(System.out::println);
        System.out.println("Oldest employee in the organization: ");
        Employee employee = employeeList.stream().max(Comparator.comparingInt(Employee::getAge)).get();
        System.out.println("Name: " + employee.getName() + " " + "Age: " + employee.getAge() + " " + "Department: " + employee.getDepartment());

        Product product1 = new Product("Electronics", "Iphone", 48000, "A");
        Product product2 = new Product("Electronics", "Iphone", 48000, "A");
        Product product3 = new Product("Electronics", "Iphone", 48000, "A");
        List<Product> list=Arrays.asList(product1,product2,product3);
        System.out.println(list.size());
    }
}
