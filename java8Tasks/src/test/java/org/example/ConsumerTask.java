package org.example;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public class ConsumerTask {
    public static void main(String[] args) throws IOException {
        ProductStore productStoreObject = new ProductStore();
        List<Product> productList = productStoreObject.getProducts();

        Product product = new Product();
        String printParameter="file";
        Consumer<Product>consumer=new Consumer<Product>() {
            @Override
            public void accept(Product product) {
                if(printParameter.equals("file")){
                    Logger logger=Logger.getLogger("Log");
                    try{
                        FileHandler fileHandler=new FileHandler("task2.txt");
                        logger.addHandler(fileHandler);
                        logger.info(product.toString());
                    } catch (Exception exception) {
                        System.out.println(exception);
                    }
                }
                else{
                    System.out.println(product);
                }
            }
        };
        productList.stream().forEach(consumer);

        System.out.println(productList.stream().filter(emp->emp.getGrade().equals("B")).map(emp->emp.getPrice())
                .mapToDouble(i->i).sum());
        List<Integer>list= Arrays.asList(1,2,3);
        System.out.println(list.stream().mapToInt(e->e*e).sum());

        System.out.println("Updating grade of the products as Premium: ");
        Consumer<Product> consumer2 = product1 -> {

            if (product1.getPrice() > 1000) product1.setGrade("Premium");
        };
        Product prod = new Product();
        consumer2.accept(prod);
        productList.stream().peek(consumer2).forEach(System.out::println);

        System.out.println("Updating name of the products(adding *to their suffixes: ");
        Consumer<Product> consumerObject = tempProduct -> {
            if (tempProduct.getPrice() > 3000) tempProduct.setProductName(tempProduct.getProductName() + "*");
        };
        productList.stream().peek(consumerObject).forEach(System.out::println);

        System.out.println("All premium graded products with suffix * are: ");
        productList.stream().filter(productSample -> productSample.getPrice() > 3000 && productSample.getGrade().equals("Premium")).peek(System.out::println);
    }

}



