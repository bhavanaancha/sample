package org.example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

public class BiFunctionTask {
    public static void main(String[] args) {
        List<Product> productList = ProductStore.getProducts();

        //Given the name and price of the product, write a Bifunction to create a product.

        BiFunction<String, Double, Product> product = (name, price) -> new Product("Electronics", name, price, "B") {
        };
        Product productObject = product.apply("mobile", 25000.00);
        System.out.println(productObject);

        //Given the Product and quantity of the products, write a Bi-Function to calculate the cost of products.
        //A cart is a map of product and quantity. Given the cart, calculate the cost of the cart.

        BiFunction<String, Integer, Double> cost = (name, quantity) ->
        {
            return productList.stream().filter(emp -> emp.getProductName().equals(name))
                    .collect(Collectors.summingDouble(price->price.getPrice()*quantity));

        };
        System.out.println(cost.apply("Fast-Track Watch", 4));

        Map<String, Integer> map = new HashMap<>();
        map.put("Reebok Shoes", 3);
        map.put("Baby SunGlasses", 2);
        map.put("Indoor Playground Set", 4);
        map.put("MacBook", 5);

        Function<Map<String, Integer>, Double> cartCost = (cartMap) -> {
            return productList.stream()
                    .filter(prod->cartMap.containsKey(prod.getProductName()))
                    .reduce(0.0,(acc,price)->
                            acc+(price.getPrice()*cartMap.get(price.getProductName())),(acc,price)->(acc+price));
        };
        System.out.println("Total cost of cart: "+cartCost.apply(map));


    }
}

