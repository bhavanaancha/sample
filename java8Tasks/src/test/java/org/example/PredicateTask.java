package org.example;

import java.util.List;
import java.util.function.Predicate;

public class PredicateTask {

    public static void main(String[] args) {
        ProductStore productStoreObject=new ProductStore();
        List<Product>productList=productStoreObject.getProducts();

        System.out.println("Products whose price is above 1000 rupees are: ");

        Predicate<Product> priceAboveThousand=product->product.getPrice()>1000;
        productList.stream().filter(priceAboveThousand)
               .forEach(System.out::println);

        System.out.println("Products of electronics category are: ");
        Predicate<Product> electronicsCategory=product -> product.getCategory().equals("Electronics");
        productList.stream().filter(electronicsCategory).forEach(category-> System.out.println(category.getProductName()));

        System.out.println("Products which are above  100/- and in Electronics category are: ");
        productList.stream().filter(product->product.getCategory().equals("Electronics")&& product.getPrice()>100)
                .forEach(System.out::println);

        System.out.println("Products which are above  100/- or in Electronics category are: ");
        productList.stream().filter(product->product.getCategory().equals("Electronics")|| product.getPrice()>100)
                .forEach(System.out::println);

        System.out.println("Products which are below  100/- and in Electronics category are: ");
        productList.stream().filter(product->product.getCategory().equals("Electronics")&& product.getPrice()<100)
                .forEach( System.out::println);

    }

}
