package org.example;

import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

public class supplier {
    public void produceRandomProduct(List<Product> productList){
    Supplier<Product> randomProduct=new Supplier<Product>(){

        @Override
        public Product get() {
            Product product=productList.get(new Random().nextInt(17));
            return product;
        }
    };
        System.out.println("random product");
        System.out.println(randomProduct.get());
        System.out.println();
}
public void produceProductOtp(){
    Supplier<Integer>randomOtp=()->new Random().nextInt(1000000);
    System.out.println("otp: ");
    System.out.println(randomOtp.get());
    }

    public static void main(String[] args) {
        ProductStore productStoreObject=new ProductStore();
        List<Product> productList=productStoreObject.getProducts();
        supplier supplierObject=new supplier();
        supplierObject.produceProductOtp();
        supplierObject.produceRandomProduct(productList);
    }
}
