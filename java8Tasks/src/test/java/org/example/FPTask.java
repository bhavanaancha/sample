package org.example;


import java.util.List;
import java.util.stream.Collectors;

interface Cost{
    int findCost(Product product);
}
interface CostOfProductsAboveFiveThousand{
    int findCostOfTheProductsAbove5k(Product product);
}

public class FPTask {

    public static void main(String[] args) {
        ProductStore productStoreObject = new ProductStore();
        List<Product> productList = productStoreObject.getProducts();
        System.out.println("Cost of all the products: ");
        System.out.println(productList.stream()
                .collect(Collectors.summingDouble(product->product.getPrice())));

        //System.out.println(costObject.findCost(new Product()));
        System.out.println("Cost of all the products whose price is above 5000/- is: ");
        System.out.println(productList.stream().filter(product2->product2.getPrice()>5000)
                    .collect(Collectors.summingDouble(product2->product2.getPrice())));
        System.out.println("Cost of all Electronics products is: ");
        System.out.println(productList.stream().filter(prod->prod.getCategory().equals("Electronics"))
                .collect(Collectors.summingDouble(product->product.getPrice())));
        System.out.println("Cost of the Electronics products whose price is above 1000/- is: ");
        System.out.println(productList.stream().filter(prod->prod.getCategory()
                .equals("Electronics") && prod.getPrice()>1000)
                .collect(Collectors.summingDouble(prod->prod.getPrice())));

    }
}