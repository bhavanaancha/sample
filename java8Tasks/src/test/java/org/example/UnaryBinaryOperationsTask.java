package org.example;

import java.util.Random;
import java.util.function.IntConsumer;
import java.util.function.IntPredicate;
import java.util.function.IntSupplier;

public class UnaryBinaryOperationsTask {
    public static void main(String[] args) {
        //1. Write an IntPredicate to verify if the given number is a primenumber

        IntPredicate primeNumber = (number) -> {
            if(number<2) return false;
            for (int i = 2; i < number; i++) {
                if (number % i == 0) return true;
            }
            return false;
        };
        boolean isPrime = primeNumber.test(30);
        if (!isPrime) System.out.println("Not a Prime number");
        else System.out.println("Prime number");

        //Write an IntConsumer to print square of the given number
        IntConsumer square = n -> System.out.println(n * n);
        square.accept(6);


        // Write a IntSupplier to give random int below 5000.
        IntSupplier randomNumber = () -> new Random().nextInt(5000);
        System.out.println(randomNumber.getAsInt());
    }
}
